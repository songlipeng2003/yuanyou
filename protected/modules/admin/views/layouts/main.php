<!doctype html>
<html ng-app="myApp">
<head>
    <meta charset="utf-8"/>
    <meta name="language" content="en" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ng-grid.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/lib/json3.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/lib/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/lib/ng-grid-2.0.7.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/app.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/common.js"></script>
</head>
<body class="container" ng-app="myApp" id="ng-app">
    <div class="row">
        <div class="span3 left_side">
            <div class="logo">
                <img src="/images/logo_igogo.png">
            </div>
            <div class="menus">
                <ul>
                    <?php if(Yii::app()->user->getState('user_level')==1){ ?>
                    <li class="menu">
                        <?php echo CHtml::link('帐号管理', array('admins/index'), array('class'=>'icon_account')); ?>
                    </li>
                    <?php } ?>
                    <?php if(Yii::app()->user->getState('user_level')<=2){ ?>
                    <li class="menu">
                        <?php echo CHtml::link('广告管理', array('ads/index'), array('class'=>'icon_advertisement')); ?>
                    </li>
                    <li class="menu">
                        <?php echo CHtml::link('最新分享', array('shares/index'), array('class'=>'icon_share')); ?>
                    </li>
                    <li class="menu">
                        <?php echo CHtml::link('最新旅游', array('posts/index'), array('class'=>'icon_travel')); ?>
                    </li>
                    <li class="menu">
                        <?php echo CHtml::link('商品管理', array('productPackages/index'), array('class'=>'icon_product')); ?>
                    </li>
                    <li class="menu">
                        <?php echo CHtml::link('订单查询', array('orders/index'), array('class'=>'icon_order')); ?>
                    </li>
                    <?php } ?>
                    <li class="menu">
                        <?php echo CHtml::link('查账', array('orders/stat'), array('class'=>'icon_audit')); ?>
                    </li>
                    <?php if(Yii::app()->user->getState('user_level')<=2){ ?>
                    <li class="menu">
                        <?php echo CHtml::link('国家列表管理', array('countries/index'), array('class'=>'icon_country')); ?>
                    </li>
                    <li class="menu">
                        <?php echo CHtml::link('用户账户管理', array('accounts/index')); ?>
                    </li>
                    <li class="menu">
                        <?php echo CHtml::link('意见反馈', array('feedbacks/index')); ?>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>                 
        <div class="span9 right_side">
            <div class="header">
                <?php echo CHtml::link('首页', array('/admin/'), array('class'=>'home')); ?>

                <div class="dropdown pull-right my_info">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo Yii::app()->user->name; ?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><?php echo CHtml::link('注销', array('/site/logout')) ?></li>
                    </ul>
                </div>
            </div>

            <div class="content">
                <?php if(Yii::app()->user->hasFlash('error')):?>
                    <div class="alert alert-error">
                        <?php echo Yii::app()->user->getFlash('error'); ?>
                    </div>
                <?php endif; ?>
                <?php if(Yii::app()->user->hasFlash('success')):?>
                    <div class="alert alert-success">
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</body>
</html>
