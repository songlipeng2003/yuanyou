<?php
/* @var $this HotsController */

$this->breadcrumbs=array(
	'Hots',
);
?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'pager' => array(
        'maxButtonCount' => '10',
    ),
    'columns' => array(
        'id',
        'country',
        'pos',
        'en_name',
        'cn_name',
        'create_at',
        array(
            'class'=> 'CButtonColumn',
            'header' =>'操作',
            'deleteConfirmation' => '确定删除？',
        ),
    )
));
