<?php
/* @var $this HotsController */

$this->breadcrumbs=array(
	'Hots'=>array('/admin/hots'),
	'Create',
);
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ad-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    <p class="note">带 <span class="required">*</span> 必须填写.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'country'); ?>
        <?php echo $form->textField($model,'country'); ?>
        <?php echo $form->error($model,'country'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'pos'); ?>
        <?php echo $form->textField($model,'pos'); ?>
        <?php echo $form->error($model,'pos'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'en_name'); ?>
        <?php echo $form->textField($model,'en_name',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'en_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cn_name'); ?>
        <?php echo $form->textField($model,'cn_name',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'cn_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'pic'); ?>
        <?php echo $form->textField($model,'pic',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'pic'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('添加'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
