<?php
/* @var $this SharesController */

$this->breadcrumbs=array(
	'Shares'=>array('/admin/shares'),
	'Create',
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'share-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
    <fieldset>
     
        <legend>创建分享</legend>

        <p class="note">带 <span class="required">*</span> 必须填写.</p>

        <?php echo $form->errorSummary($model); ?>

        <?php 
            echo $form->textFieldRow($model, 'name');
            echo $form->dropDownListRow($model, 'country', $countries);
        ?>
        <div class="control-group">
            <label class="control-label">图片</label>
            <div id="images" class="controls">
                <div id="uploadify">
                    <input type="file" name="file" id="upload">
                </div>
                <ul class="thumbnails" id="image">
                </ul>
            </div>
        </div>
        <?php
            echo $form->hiddenField($model, 'pic');
            echo $form->checkboxRow($model, 'is_hot');
            echo $form->ckEditorRow($model, 'content', array(
                'editorOptions' => array(
                    'fullpage' => 'js:true',
                    'width' => '500',
                    'plugins' => 'basicstyles,toolbar,enterkey,entities,floatingspace,wysiwygarea,indentlist,link,list,dialog,dialogui,button,indent,fakeobjects'
                )
            ));
        ?>
 
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'提交')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消')); ?>
    </div>

<?php $this->endWidget(); ?>

<script id="image-item" type="text/x-jsrender">
    <li class="span4">
        <img src="{{:url}}"/>
    </li>
</script>

<script type="text/javascript" src="/javascripts/lib/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="/javascripts/lib/jsrender.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('#upload').uploadify({
            'swf':'/swf/uploadify.swf',
            'uploader':'/file/upload',
            'buttonText':'上传',
            'fileTypeExts':'*.jpg;*.png;*.gif',
            'multi':false,
            'onUploadSuccess':function(file,data,response){
                var data = jQuery.parseJSON(data);
                var html = $("#image-item").render(data);
                if(data.code==0){
                    $('#ShareForm_pic').val(data.path);
                    $('#image').html(html);
                }
            }
        });
    });
</script>