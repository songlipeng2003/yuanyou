<div ng-controller="sharesController">
    <form class="form-search">
        时间：
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'beg', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'class'=>'span2',
                'ng-model'=>'filter.beg'
            ))); ?>
        -
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'end', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'class'=>'span2',
                'ng-model'=>'filter.end'
            ))); ?>
        国家：
        <select ng-model="filter.type" ng-options="key as value for (key, value) in countries" class="span2">
            <option value="0">全部</option>
        </select>
        状态：
        <select ng-model="filter.type" class="span2">
            <option value="0">全部</option>
            <option value="1">通过核审的</option>
            <option value="2">未通过核审的</option>
            <option value="3">精华</option>
        </select>
    </form>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="share_edit_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看分享</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">标题：</label>
                            <div class="controls">
                                <p ng-bind="share.name"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">国家：</label>
                            <div class="controls">
                                <p>{{countries[share.country]}}</p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">图片：</label>
                            <div class="controls">
                                <img ng-show="share.pic!=''" ng-src="<?php echo ApiUtil::BASE_URL ?>{{share.pic}}"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">是否精华：</label>
                            <div class="controls">
                                <p ng-bind="share.is_hot"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">发布者：</label>
                            <div class="controls">
                                <p ng-bind="share.creator"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">发布时间：</label>
                            <div class="controls">
                                <p ng-bind="share.create_at"></p>
                            </div>
                        </div>
                        <!--
                        <div class="control-group">
                            <label class="control-label">内容：</label>
                            <div class="controls col-sm-9">
                                <p>
                                    <iframe ng-src="<?php echo ApiUtil::BASE_URL ?>/share/article-{{share.id}}"></iframe>
                                </p>
                            </div>
                        </div>
                        -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" ng-click="valid(share)">审核通过</button>
                    <button type="button" ng-show="!share.is_hot" class="btn btn-primary" ng-click="set_hot(share)">设置精华</button>
                    <button type="button" ng-show="share.is_hot" class="btn btn-primary" ng-click="set_not_hot(share)">设置非精华</button>
                    <button type="button" class="btn btn-primary" ng-click="remove(share)">删除</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal hide" id="share_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看分享</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" name="id" value="" ng-model="share.id">
                        <div class="control-group">
                            <label class="control-label">标题：</label>
                            <div class="controls">
                                <p ng-bind="share.name"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">国家：</label>
                            <div class="controls">
                                <p>{{countries[share.country]}}</p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">图片：</label>
                            <div class="controls">
                                <img ng-show="share.pic!=''" ng-src="<?php echo ApiUtil::BASE_URL ?>{{share.pic}}"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">是否精华：</label>
                            <div class="controls">
                                <p ng-bind="share.is_hot"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">发布者：</label>
                            <div class="controls">
                                <p ng-bind="share.creator"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">发布时间：</label>
                            <div class="controls">
                                <p ng-bind="share.create_at"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">内容：</label>
                            <div class="controls col-sm-9">
                                <p ng-html-bind="share.content"></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/sharesController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/shareService.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/countryService.js"></script>