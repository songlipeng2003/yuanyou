<div ng-controller="orderStatController">
    <form class="form-search">
        国家：
        <select ng-model="filter.country" ng-options="key as value for (key, value) in countries" class="span2">
            <option value="">全部</option>
        </select>
        时间:
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'beg', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'class'=>'span2',
                'ng-model'=>'filter.beg'
            ))); ?>
        -
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'end', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'class'=>'span2',
                'ng-model'=>'filter.end'
            ))); ?>

        <button type="button" class="btn btn-default pull-right" ng-click="export()">导出数据</button>
    </form>
    <div class="data-grid" ng-grid="gridOptions"></div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/orderStatController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/countryService.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/orderService.js"></script>