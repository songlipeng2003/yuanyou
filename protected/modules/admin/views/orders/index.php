<div ng-controller="ordersController">
    <form class="form-search">
        用户名：
        <input ng-model="filter.uid" class="span1"/>
        订单号：
        <input ng-model="filter.orderid" class="span2"/>
        时间：
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'beg', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'style'=>'width:100px;',
                'ng-model'=>'filter.beg'
            ))); ?>
        -
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'end', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'style'=>'width:100px;',
                'ng-model'=>'filter.end'
            ))); ?>
    </form>
    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="order_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看订单</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">订单号：</label>
                            <div class="controls">
                                <p ng-bind="order.id"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">产品名：</label>
                            <div class="controls">
                                <p ng-bind="order.product.name"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">单价：</label>
                            <div class="controls">
                                <p ng-bind="order.price"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">订单数量：</label>
                            <div class="controls">
                                <p ng-bind="order.count"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">创建时间：</label>
                            <div class="controls col-sm-9">
                                <p ng-bind="order.create_at"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">支付时间：</label>
                            <div class="controls col-sm-9">
                                <p ng-bind="order.pay_at"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">订单状态：</label>
                            <div class="controls col-sm-9">
                                <p >{{stats[order.stat]}}</p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">订单描述：</label>
                            <div class="controls col-sm-9">
                                <p ng-bind="order.desc"></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/ordersController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/orderService.js"></script>