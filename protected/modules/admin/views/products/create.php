<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'product-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
    <fieldset>
        <legend>商品管理-新增商品</legend>

        <p class="note">带 <span class="required">*</span> 必须填写.</p>

        <?php echo $form->errorSummary($model); ?>

        <?php 
            echo $form->textFieldRow($model, 'name');
            echo $form->dropDownListRow($model, 'country', $countries);
            echo $form->textFieldRow($model, 'price');
            echo $form->textAreaRow($model, 'desc', array('class'=>'span4', 'rows'=>10));
        ?>
 
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'提交')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消')); ?>
    </div>

<?php $this->endWidget(); ?>