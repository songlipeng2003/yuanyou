<div ng-controller="productsController">
    <div class="page-header">
        <?php echo CHtml::link('新增商品', array('products/create'), array('class'=>'btn')) ?>
    </div>

    <form class="form-search">
        <select ng-model="filter.stat">
            <option value="0">所有产品</option>
            <option value="1">未上架</option>
            <option value="2">上架</option>
            <option value="3">下架</option>
        </select>
    </form>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="product_edit_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">编辑产品</h4>
                </div>

                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'product-form',
                    'type'=>'horizontal',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); ?>
                    <div class="modal-body">
                        <fieldset>
                            <legend>商品管理-新增商品</legend>

                            <p class="note">带 <span class="required">*</span> 必须填写.</p>

                            <?php echo $form->errorSummary($model); ?>

                            <?php 
                                echo $form->textFieldRow($model, 'name', array(
                                 'ng-model'=>"product.name"
                            ));
                                echo $form->dropDownListRow($model, 'country', $countries, array(
                                 'ng-model'=>"product.country"
                            ));
                                echo $form->textFieldRow($model, 'price', array(
                                 'ng-model'=>"product.price"
                            ));
                                echo $form->textAreaRow($model, 'desc', array('class'=>'span4', 'rows'=>10, 
                                 'ng-model'=>"product.desc"
                            ));
                            ?>
                     
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'primary', 'label'=>'提交', 'htmlOptions'=>array("ng-click"=>"update(share)"))); ?>
                        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array("data-dismiss"=>"modal"))); ?>
                    </div>
                <?php $this->endWidget(); ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal hide" id="product_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看产品</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" name="id" value="" ng-model="product.id">
                        <div class="control-group">
                            <label class="control-label">商品名称：</label>
                            <div class="controls">
                                <p ng-hide="is_edit" ng-bind="product.name"></p>
                                <input ng-show="is_edit" type="text" class="form-control" ng-model="product.name">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">价格：</label>
                            <div class="controls">
                                <p ng-hide="is_edit" ng-bind="product.price"></p>
                                <input ng-show="is_edit" type="text" class="form-control" ng-model="product.price">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">状态：</label>
                            <div class="controls">
                                <p ng-hide="is_edit" ng-bind="product.stat"></p>
                                <input ng-show="is_edit" type="text" class="form-control" ng-model="product.stat">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">国家：</label>
                            <div class="controls">
                                <p ng-hide="is_edit" ng-bind="product.country"></p>
                                <select ng-show="is_edit" ng-model="product.country" ng-options="key as value for (key, value) in countries"></select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">内容：</label>
                            <div class="controls col-sm-9">
                                <p ng-hide="is_edit" ng-bind="product.desc"></p>
                                <textarea ng-show="is_edit" type="text" class="form-control" ng-model="product.desc"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" ng-hide="is_edit" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" ng-show="is_edit" ng-click="update(product)">保存</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/productsController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/countryService.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/productService.js"></script>