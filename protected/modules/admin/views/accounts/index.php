<div ng-controller="accountsController">
    <form class="form-search">
        编号：<input ng-model="filter.sid" />
        用户名：<input ng-model="filter.uid" />
    </form>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="account_modal" ng-if="account">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">查看账户</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">用户名：</label>
                            <div class="controls">
                                <p ng-bind="account.uid"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">设备码：</label>
                            <div class="controls">
                                <p ng-bind="account.last_login_dev"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
                                <p ng-bind="account.email"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">手机号：</label>
                            <div class="controls">
                                <p ng-bind="account.phone_no"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">状态：</label>
                            <div class="controls">
                                <span>{{is_locks[account.is_lock]}}</span>
                                <button ng-click="unlock(account)" ng-show="account.is_lock" type="button" class="btn btn-default">激活</button>
                                <button ng-click="lock(account)" ng-show="!account.is_lock" type="button" class="btn btn-default">关闭</button>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">登录日志：</label>
                            <div class="controls">
                                <p ng-repeat="log in logs">
                                    {{log.dt}} 设备:{{log.dev}} 设备码:{{log.code}}
                                </p>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/accountsController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/accountService.js"></script>