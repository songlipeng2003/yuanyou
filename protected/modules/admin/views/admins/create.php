<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'admin-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
    <fieldset>
        <legend>账户管理-创建账户</legend>

        <p class="note">带 <span class="required">*</span> 必须填写.</p>

        <?php echo $form->errorSummary($model); ?>

        <?php 
            echo $form->textFieldRow($model, 'username');
            echo $form->passwordFieldRow($model, 'pwd');
            echo $form->passwordFieldRow($model, 'confirm_pwd');
            echo $form->radioButtonListRow($model, 'level', array(2=>'二级账户',3=>'三级账户'));
            echo $form->checkBoxListRow($model, 'country', $countries);
        ?>
 
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'提交')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array('data-href'=>'/admin/admins'))); ?>
    </div>

<?php $this->endWidget(); ?>