<div ng-controller="adminsController">
    <div class="page-header">
        <?php echo CHtml::link('创建新帐号', array('admins/create'), array('class'=>'btn')) ?>
    </div>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="admin_modal" ng-if="admin">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看账户</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">用户名：</label>
                            <div class="controls">
                                <p ng-bind="admin.username"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">等级：</label>
                            <div class="controls">
                                <p>
                                    <span ng-show="admin.level==2">二级账户</span>
                                    <span ng-show="admin.level==3">三级账户</span>
                                </p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">国家：</label>
                            <div class="controls">
                                <p>
                                    <span ng-repeat="country in admin.country_list|split">{{countries[country]}}<br/></span>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/adminsController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/countryService.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/adminService.js"></script>