<div ng-controller="feedbacksController">
    <form class="form-search">
        状态：
        <select ng-model="filter.op">
            <option value="0">全部</option>
            <option value="1">未处理</option>
            <option value="2">已处理</option>
        </select>
        时间：
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'beg', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'ng-model'=>'filter.beg',
                'class'=>'span2'
            ))); ?>
        -
        <?php $this->widget('bootstrap.widgets.TbDatePicker', array(
            'name'=>'end', 
            'options'=>array(
                'language'=>'zh-CN',
                'format'=>'yyyy-mm-dd'
            ),
            'htmlOptions'=>array(
                'ng-model'=>'filter.end',
                'class'=>'span2'
            ))); ?>
        <button type="button" class="btn btn-default pull-right" ng-click="export()">导出数据</button>
    </form>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="feedback_edit_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看反馈</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">用户名：</label>
                            <div class="controls">
                                <p ng-bind="feedback.username"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">设备：</label>
                            <div class="controls">
                                <p ng-bind="feedback.devinfo"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">软件版本：</label>
                            <div class="controls">
                                <p ng-bind="feedback.sw_ver"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">数据版本：</label>
                            <div class="controls">
                                <p ng-bind="feedback.data_ver"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">联系方式：</label>
                            <div class="controls">
                                <p ng-bind="feedback.connect"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">内容：</label>
                            <div class="controls">
                                <p ng-bind="feedback.content"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">处理结果</label>
                            <div class="controls">
                                <input ng-model="feedback.op" required/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">发布时间：</label>
                            <div class="controls">
                                <p ng-bind="feedback.create_at"></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'primary', 'label'=>'提交', 'htmlOptions'=>array("ng-click"=>"update(feedback)"))); ?>
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array("data-dismiss"=>"modal"))); ?>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal hide" id="feedback_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看反馈</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">用户名：</label>
                            <div class="controls">
                                <p ng-bind="feedback.username"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">设备：</label>
                            <div class="controls">
                                <p ng-bind="feedback.devinfo"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">软件版本：</label>
                            <div class="controls">
                                <p ng-bind="feedback.sw_ver"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">数据版本：</label>
                            <div class="controls">
                                <p ng-bind="feedback.data_ver"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">联系方式：</label>
                            <div class="controls">
                                <p ng-bind="feedback.connect"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">状态：</label>
                            <div class="controls">
                                <p ng-bind="feedback.op_stat"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">内容：</label>
                            <div class="controls">
                                <p ng-bind="feedback.content"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">处理结果</label>
                            <div class="controls">
                                <p ng-bind="feedback.op"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">发布时间：</label>
                            <div class="controls">
                                <p ng-bind="feedback.create_at"></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/feedbacksController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/feedbackService.js"></script>