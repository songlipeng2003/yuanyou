<div ng-controller="countriesController">
    <div class="page-header">
        <?php echo CHtml::link('新建国家', array('countries/create'), array('class'=>'btn')) ?>
    </div>

    <div class="data-grid" ng-grid="gridOptions"></div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/countriesController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/countryService.js"></script>