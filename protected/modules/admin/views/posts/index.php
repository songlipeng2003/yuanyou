<div ng-controller="postsController">
    <div class="page-header">
        <?php echo CHtml::link('新增旅游', array('posts/create'), array('class'=>'btn')) ?>
    </div>

    <form class="form-search">
        <select ng-model="filter.country" ng-options="key as value for (key, value) in countries">
            <option value="">全部</option>
        </select>
    </form>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="post_edit_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">编辑旅游</h4>
                </div>
                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'post-form',
                    'type'=>'horizontal',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); ?>
                    <div class="modal-body">
                        <fieldset>
                            <p class="note">带 <span class="required">*</span> 必须填写.</p>

                            <?php echo $form->errorSummary($model); ?>

                            <input type="hidden" name="id" value="" ng-value="post.id">

                            <?php 
                                echo $form->textFieldRow($model, 'title', array(
                                 'ng-model'=>"post.title"
                            ));
                                echo $form->dropDownListRow($model, 'country', $countries, array(
                                 'ng-model'=>"post.country"
                            ));
                            ?>
                            <div class="control-group">
                                <label class="control-label">图片</label>
                                <div id="images" class="controls">
                                    <div id="uploadify">
                                        <input type="file" name="file" id="upload">
                                    </div>
                                    <ul class="thumbnails" id="image">
                                        <li class="thumbnail span2" ng-repeat="img in post.pic_url|split:';'">
                                            <button type="button" class="close" onclick="return remove_image(this)">×</button>
                                            <img ng-src="<?php echo ApiUtil::BASE_URL ?>{{img}}" />
                                            <input type="hidden" name="PostForm[image][]" value="{{img}}">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php
                                echo $form->textAreaRow($model, 'content', array(
                                    'class'=>'span4', 
                                    'rows'=>10,
                                    'ng-model'=>"post.content"
                                ));
                            ?>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'primary', 'label'=>'提交', 'htmlOptions'=>array("ng-click"=>"update(post)"))); ?>
                        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array("data-dismiss"=>"modal"))); ?>
                    </div>

                <?php $this->endWidget(); ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal hide" id="post_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看旅游</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" name="id" value="" ng-model="post.id">
                        <div class="control-group">
                            <label class="control-label">标题：</label>
                            <div class="controls">
                                <p ng-bind="post.title"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">国家：</label>
                            <div class="controls">
                                <p>{{countries[post.country]}}</p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">图片：</label>
                            <div class="controls">
                                <p>
                                    <ul class="thumbnails">
                                        <li class="thumbnail span2" ng-repeat="img in post.pic_url|split:';'">
                                            <img ng-src="<?php echo ApiUtil::BASE_URL ?>{{img}}" />
                                        </li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">内容：</label>
                            <div class="controls col-sm-9">
                                <p ng-bind="post.content"></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/postsController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/countryService.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/postService.js"></script>

<script id="image-item" type="text/x-jsrender">
    <li class="span2">
        <button type="button" class="close" onclick="return remove_image(this)">×</button>
        <img src="{{:url}}"/>
        <input type="hidden" name="PostForm[image][]" value="{{:path}}">
    </li>
</script>

<script type="text/javascript" src="/javascripts/lib/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="/javascripts/lib/jsrender.min.js"></script>
<script type="text/javascript">
$(function(){
    $('#upload').uploadify({
        'swf':'/swf/uploadify.swf',
        'uploader':'/file/upload',
        'buttonText':'上传',
        'fileTypeExts':'*.jpg;*.png;*.gif',
        'onUploadSuccess':function(file,data,response){
            var data = jQuery.parseJSON(data);
            var html = $("#image-item").render(data);
            if(data.code==0){
                $('#images .thumbnails').append(html);
            }
        }
    });
});

function remove_image($this){
    $($this).parent().remove();
    return false;
}

</script>