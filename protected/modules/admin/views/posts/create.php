<?php
/* @var $this PostsController */

$this->breadcrumbs=array(
	'Posts'=>array('/admin/posts'),
	'Create',
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'post-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
    <fieldset>
     
        <legend>创建旅游</legend>

        <p class="note">带 <span class="required">*</span> 必须填写.</p>

        <?php echo $form->errorSummary($model); ?>

        <?php 
            echo $form->textFieldRow($model, 'title');
            echo $form->dropDownListRow($model, 'country', $countries);
        ?>

        <div class="control-group">
            <label class="control-label">图片</label>
            <div id="images" class="controls">
                <div id="uploadify">
                    <input type="file" name="file" id="upload">
                </div>
                <ul class="thumbnails" id="images-grid">
                </ul>
            </div>
        </div>

        <?php
            echo $form->textAreaRow($model, 'content', array('class'=>'span4', 'rows'=>10));
        ?>
 
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'提交')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array('data-href'=>'/admin/posts'))); ?>
    </div>

<?php $this->endWidget(); ?>

<script id="image-item" type="text/x-jsrender">
    <li class="span1">
        <img src="{{:url}}"/>
        <input type="hidden" name="PostForm[image][]" value="{{:path}}">
    </li>
</script>

<script type="text/javascript" src="/javascripts/lib/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="/javascripts/lib/jsrender.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('#upload').uploadify({
            'swf':'/swf/uploadify.swf',
            'uploader':'/file/upload',
            'buttonText':'上传',
            'fileTypeExts':'*.jpg;*.png;*.gif',
            'onUploadSuccess':function(file,data,response){
                var data = jQuery.parseJSON(data);
                var html = $("#image-item").render(data);
                if(data.code==0){
                    $('#images-grid').append(html);
                }
            }
        });
    });
</script>
