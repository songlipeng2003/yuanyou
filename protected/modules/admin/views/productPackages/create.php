<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'product-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
    <fieldset>
        <legend>商品管理-新增商品</legend>

        <p class="note">带 <span class="required">*</span> 必须填写.</p>

        <?php echo $form->errorSummary($model); ?>

        <?php 
            echo $form->textFieldRow($model, 'name');
            echo $form->textFieldRow($model, 'type');
            echo $form->dropDownListRow($model, 'country', $countries);
        ?>

        <div class="control-group">
            <label class="control-label required">
                使用权限 <span class="required">*</span>
            </label>
            <div class="controls">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#add_product_modal">增加使用权限</button>
                <div id="products_list">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="100">权限</th>
                                <th width="200">价格（元）</th>
                                <th width="180">操作</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
 
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'提交')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array('data-href'=>'/admin/productPackages'))); ?>
    </div>

<?php $this->endWidget(); ?>

<div class="modal hide fade" id="add_product_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">查看商品列表</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <input type="hidden" name="id" value="" ng-model="product_package.id">
                    <div class="control-group">
                        <label class="control-label">权限：</label>
                        <div class="controls">
                            <select id="months">
                                <option value="1">一个月期限</option>
                                <option value="2">两个月期限</option>
                                <option value="3">三个月期限</option>
                                <option value="4">四个月期限</option>
                                <option value="5">五个月期限</option>
                                <option value="6">六个月期限</option>
                                <option value="7">七个月期限</option>
                                <option value="8">八个月期限</option>
                                <option value="9">九个月期限</option>
                                <option value="10">十个月期限</option>
                                <option value="11">十一个月期限</option>
                                <option value="12">一年期限</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</button>
                <button type="button" class="btn btn-primary" id="add_product">添加</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal hide fade" id="add_desc_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">添加文字说明</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <input type="hidden" name="id" value="" ng-model="product_package.id">
                    <div class="control-group">
                        <label class="control-label">说明：</label>
                        <div class="controls">
                            <textarea id="product_desc" class="span3" rows="5"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">关闭</button>
                <button type="button" class="btn btn-primary" id="add_desc">添加</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$(function(){
    $('#add_product').click(function(){
        var value = $('#months').val();
        var name = $('#months option:selected').text();
        var html = '<tr>' +
            '<td>' + name + '</td>' +
            '<td><input type="hidden" name="name[]" value="' + name + '"/><input type="hidden" name="month[]" value="' + value + '"/><input type="input" name="price[]" /><input type="hidden" name="desc[]" /></td>' +
            '<td><button type="button" class="btn btn-primary" onclick="return delete_product(this)">删除</button> ' +
            '<button type="button" class="btn btn-primary" onclick="return set_desc(this)">添加文字说明</button></td>' +
            '</tr>';
        $('#products_list table').append(html);

        $('#add_product_modal').modal('hide');
    });
});

function delete_product(e){
    $(e).parents('tr').remove();
    return false;
}

function set_desc(e){
    $('#add_desc_modal').modal('show');

    $('#product_desc').val($(e).parents('tr').find('input[name="desc[]"]').val());

    $('#add_desc').unbind('click').click(function(){
        $(e).parents('tr').find('input[name="desc[]"]').val($('#product_desc').val());
        $('#product_desc').val('');
        $('#add_desc_modal').modal('hide');
    });
}
</script>