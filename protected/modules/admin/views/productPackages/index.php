<div ng-controller="productPackagesController">
    <div class="page-header">
        <?php echo CHtml::link('新增商品', array('productPackages/create'), array('class'=>'btn')) ?>
    </div>

    <form class="form-search">
        标签:<input ng-model="filter.type">
        国家:
        <select ng-model="filter.country" ng-options="key as value for (key, value) in countries">
            <option value="">全部</option>
        </select>
    </form>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="product_package_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看商品列表</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" name="id" value="" ng-model="product_package.id">
                        <div class="control-group">
                            <label class="control-label">列表名称：</label>
                            <div class="controls">
                                <p ng-bind="product_package.name"></p>
                                <input ng-show="is_edit" type="text" class="form-control" ng-model="product_package.name">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">标签：</label>
                            <div class="controls">
                                <p ng-bind="product_package.type"></p>
                                <input ng-show="is_edit" type="text" class="form-control" ng-model="product_package.price">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">国家：</label>
                            <div class="controls">
                                <p>{{countries[product_package.country]}}</p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"> 产品：</label>
                            <div class="controls">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>编号</th>
                                            <th>名称</th>
                                            <th>价格</th>
                                            <th>文字说明</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="product in product_package.products">
                                            <td ng-bind="product.id"></td>
                                            <td ng-bind="product.name"></td>
                                            <td ng-bind="product.price"></td>
                                            <td ng-bind="product.desc"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" ng-show="is_edit" ng-click="update(product_package)">保存</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/productPackagesController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/countryService.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/productPackageService.js"></script>