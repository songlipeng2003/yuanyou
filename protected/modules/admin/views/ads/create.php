<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'ad-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
    <fieldset>
        <legend>广告管理-新增广告</legend>

        <p class="note">带 <span class="required">*</span> 必须填写.</p>

        <?php echo $form->errorSummary($model); ?>

        <?php 
            echo $form->textFieldRow($model, 'title');
            echo $form->datetimepickerRow($model, 'expire_at', 
            array(
                'options'=>array(
                    'language'=>'zh-CN',
                    'format'=>'yyyy-mm-dd hh:ii:ss'
                ),
                'htmlOptions' => array(
                    'readonly'=>true
                )
            ),
            array(
                'prepend' => '<i class="icon-calendar"></i>'
            )); 
            echo $form->textFieldRow($model, 'where');
            echo $form->textFieldRow($model, 'customer_name');
            echo $form->textFieldRow($model, 'link');
            echo $form->textFieldRow($model, 'content');
        ?>

        <div class="control-group">
            <label class="control-label">图片</label>
            <div id="images" class="controls">
                <div id="uploadify">
                    <input type="file" name="file" id="upload">
                </div>

                <span class="help-block">如果上传图片会覆盖广告内容，上传后请不要修改广告内容</span>
                <ul class="thumbnails" id="image">
                </ul>
            </div>
        </div>
 
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'提交')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array('data-href'=>'/admin/ads'))); ?>
    </div>

<?php $this->endWidget(); ?>

<script id="image-item" type="text/x-jsrender">
    <li class="span4">
        <img src="{{:url}}"/>
    </li>
</script>

<script type="text/javascript" src="/javascripts/lib/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="/javascripts/lib/jsrender.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('#upload').uploadify({
            'swf':'/swf/uploadify.swf',
            'uploader':'/file/upload',
            'buttonText':'上传',
            'fileTypeExts':'*.jpg;*.png;*.gif',
            'multi':false,
            'onUploadSuccess':function(file,data,response){
                var data = jQuery.parseJSON(data);
                var html = $("#image-item").render(data);
                if(data.code==0){
                    $('#AdForm_content').val(data.path);
                    $('#image').html(html);
                }
            }
        });
    });
</script>