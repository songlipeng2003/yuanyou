<div ng-controller="adsController">
    <div class="page-header">
        <?php echo CHtml::link('新增广告', array('ads/create'), array('class'=>'btn')) ?>
    </div>

    <div class="data-grid" ng-grid="gridOptions"></div>

    <div class="modal hide" id="ad_edit_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">编辑广告</h4>
                </div>

                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'ad-form',
                    'action'=>array('ads/update'),
                    'type'=>'horizontal',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); ?>
                <div class="modal-body">
                    <fieldset>
                        <p class="note">带 <span class="required">*</span> 必须填写.</p>

                        <?php echo $form->errorSummary($model); ?>
                        <input type="hidden" name="AdForm[id]" ng-value="ad.id" />

                        <?php 
                            echo $form->textFieldRow($model, 'title', array(
                                 'ng-model'=>"ad.title"
                            ));
                            echo $form->datetimepickerRow($model, 'expire_at', 
                            array(
                                'options'=>array(
                                    'language'=>'zh-CN',
                                    'format'=>'yyyy-mm-dd hh:ii:ss'
                                ),
                                'htmlOptions' => array(
                                    'readonly'=>true,
                                    'ng-model'=>"ad.expire_at"
                                )
                            ),
                            array(
                                'prepend' => '<i class="icon-calendar"></i>'
                            )); 
                            echo $form->textFieldRow($model, 'link', array(
                                 'ng-model'=>"ad.link"
                            ));
                            echo $form->textFieldRow($model, 'where', array(
                                 'ng-model'=>"ad.where"
                            ));
                            echo $form->textFieldRow($model, 'content', array(
                                'ng-model'=>"ad.content"
                            ));
                        ?>

                        <div class="control-group">
                            <label class="control-label">图片</label>
                            <div id="images" class="controls">
                                <div id="uploadify">
                                    <input type="file" name="file" id="upload">
                                </div>

                                <span class="help-block">如果上传图片会覆盖广告内容，上传后请不要修改广告内容</span>
                                <ul class="thumbnails" id="image">
                                </ul>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'primary', 'label'=>'提交', 'htmlOptions'=>array("ng-click"=>"update(ad)"))); ?>
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'取消', 'htmlOptions'=>array("data-dismiss"=>"modal"))); ?>
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal hide" id="ad_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">查看广告</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" name="id" value="" ng-model="ad.id">
                        <div class="control-group">
                            <label class="control-label">标题：</label>
                            <div class="controls">
                                <p ng-bind="ad.title"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">链接：</label>
                            <div class="controls">
                                <p ng-bind="ad.link"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">位置：</label>
                            <div class="controls">
                                <p ng-bind="ad.where"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">客户名：</label>
                            <div class="controls">
                                <p ng-bind="ad.customer_name"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">过期时间：</label>
                            <div class="controls">
                                <p ng-bind="ad.expire_at"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">内容：</label>
                            <div class="controls col-sm-9">
                                <img ng-src="<?php echo ApiUtil::BASE_URL ?>{{ad.content}}" ng-show="ad.content.indexOf('.jpg')>0"/>
                                <p ng-bind="ad.content" ng-show="ad.content.indexOf('.jpg')<0"></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/controllers/adsController.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/javascripts/services/adService.js"></script>

<script id="image-item" type="text/x-jsrender">
    <li class="span4">
        <img src="{{:url}}"/>
    </li>
</script>

<script type="text/javascript" src="/javascripts/lib/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="/javascripts/lib/jsrender.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('#upload').uploadify({
            'swf':'/swf/uploadify.swf',
            'uploader':'/file/upload',
            'buttonText':'上传',
            'fileTypeExts':'*.jpg;*.png;*.gif',
            'multi':false,
            'onUploadSuccess':function(file,data,response){
                var data = jQuery.parseJSON(data);
                var html = $("#image-item").render(data);
                if(data.code==0){
                    $('#AdForm_content').val(data.path);
                    $('#AdForm_content').trigger('input');
                    $('#image').html(html);
                }
            }
        });
    });
</script>