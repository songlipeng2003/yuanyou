<?php

class AdsController extends Controller
{
	public function actionCreate()
	{
		$model=new AdForm();
		if(isset($_POST['AdForm']))
		{
			$model->attributes=$_POST['AdForm'];
			if($model->validate())
			{
				$user = Yii::app()->user;
				$model->creator = $user->id;
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}
		$this->render('create',array('model'=>$model));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$result = ApiUtil::api('ad/delete', array('id'=>$id));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex()
	{
		$model=new AdForm();
		$this->render('index', array(
			'model'=>$model
		));
	}

	public function actionList()
	{
		$limit = intval(Yii::app()->request->getParam('limit', 10));
		$page = intval(Yii::app()->request->getParam('page', 1));
		$skip = ($page-1)*$limit;
		$params=array(
			'skip'=>$skip, 
			'count'=>$limit
		);

		$result = ApiUtil::api('ad/list', $params);

		$this->renderJSON($result);
	}

	public function actionUpdate($id)
	{
		$model=new AdForm();
		if(isset($_POST))
		{
			$model->attributes=$_POST;
			$model->id = $id;
			if($model->validate())
			{
				if($model->update()){
					$this->renderJSON(array(
						'code'=>0
					));
				}
				$this->renderJSON(array(
					'code'=>500
				));
			}

			$this->renderJSON(array(
				'code'=>500
			));
		}
	}
}