<?php

class ProductsController extends Controller
{
	public function actionCreate()
	{
		$model=new ProductForm();
		if(isset($_POST['ProductForm']))
		{
			$model->attributes=$_POST['ProductForm'];
			if($model->validate())
			{
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('products/index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];
		
		$this->render('create',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$result = ApiUtil::api('product/remove', array('id'=>$id));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex()
	{
		$model = new ProductForm();

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		$this->render('index',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionList()
	{
		$stat = Yii::app()->request->getParam('stat', 0);
		$limit = intval(Yii::app()->request->getParam('limit', 10));
		$page = intval(Yii::app()->request->getParam('page', 1));
		$skip = ($page-1)*$limit;
		$params=array(
			'stat'=>$stat,
			'skip'=>$skip,
			'count'=>$limit
		);

		$result = ApiUtil::api('product/list', $params);

		$this->renderJSON($result);
	}

	public function actionUpdate($id)
	{
		$model=new ProductForm();
		if(isset($_POST))
		{
			$model->attributes=$_POST;
			$model->id = $id;
			if($model->validate())
			{
				if($model->update()){
					$this->renderJSON(array(
						'code'=>0
					));
				}
				$this->renderJSON(array(
					'code'=>500
				));
			}

			$this->renderJSON(array(
				'code'=>500
			));
		}
	}
}