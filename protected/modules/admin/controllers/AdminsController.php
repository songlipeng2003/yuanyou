<?php

class AdminsController extends Controller
{
	public function actionCreate()
	{
		$model=new AdminForm();
		if(isset($_POST['AdminForm']))
		{
			$model->attributes=$_POST['AdminForm'];
			if($model->validate())
			{
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		$this->render('create',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionIndex()
	{
		$model=new AdminForm();

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		$this->render('index',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionList()
	{
		$result = ApiUtil::api('admin/list');

		$this->renderJSON($result);
	}

	public function actionDelete($username)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$result = ApiUtil::api('admin/delete', array('username'=>$username));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionUpdate($username, $level, $country_list)
	{
		$model=new AdminForm();

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		if(!empty($_POST))
		{
			$model->attributes=$_POST['AdminForm'];
			if($model->validate())
			{
				if($model->update()){
					Yii::app()->user->setFlash('success', '更新成功');
					$this->redirect(array('admins/index'));
				}
			}
			Yii::app()->user->setFlash('error', '更新失败');
			$this->render('update', array(
				'model'=>$model,
				'countries'=>$countries
			));
		}else{
			$model->username = $username;
			$model->level = $level;
			$model->country = explode(',', $country_list);
			$this->render('update', array(
				'model'=>$model,
				'countries'=>$countries
			));
		}
	}
}