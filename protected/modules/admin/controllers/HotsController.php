<?php

class HotsController extends Controller
{
	public function actionCreate()
	{
		$model=new HotForm();
		if(isset($_POST['HotForm']))
		{
			$model->attributes=$_POST['HotForm'];
			if($model->validate())
			{
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('hots/index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}
		$this->render('create',array('model'=>$model));
	}

	public function actionDelete()
	{
		$this->render('delete');
	}

	public function actionIndex()
	{
		$params=array('country'=>'', 'skip'=>0, 'count'=>10);

		$dataProvider = new ApiDataProvider('hot/list', $params, array());

		$this->render('index', array(
			'dataProvider'=>$dataProvider
		));
	}

	public function actionUpdate()
	{
		$this->render('update');
	}
}