<?php

class FeedbacksController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionList()
	{
		$limit = intval(Yii::app()->request->getParam('limit', 10));
		$page = intval(Yii::app()->request->getParam('page', 1));
		$op = intval(Yii::app()->request->getParam('op', 0));
		$beg = Yii::app()->request->getParam('beg', '20000101000000');
		$end = Yii::app()->request->getParam('end', '20501230000000');
		$export = Yii::app()->request->getParam('export', false);
		$skip = ($page-1)*$limit;
		$params=array(
			'skip'=>$skip, 
			'count'=>$limit,
			'op'=>$op
		);

		if($beg){
	        $beg = strtotime($beg);
	        $beg = strftime('%Y%m%d%H%M%S', $beg);
	        $params['beg'] = $beg;
		}
		if($end){
	        $end = strtotime($end);
	        $end = strftime('%Y%m%d%H%M%S', $end);
	        $params['end'] = $end;
		}

		$result = ApiUtil::api('account/feedback/list4admin', $params);
		if($export && $result['list']){
			$this->exportArray($result['list']);
		}

		$this->renderJSON($result);
	}

	public function actionUpdate($id)
	{
		if(isset($_POST))
		{
			$op = $_POST['op'];
			$params = array(
				'id'=>$id,
				'op_stat'=>1,
				'op'=>$op
			);

			$result = ApiUtil::api('account/feedback/op', $params);

			$this->renderJSON($result);
		}
	}

	public function actionDelete($id)
	{
		if(isset($_POST))
		{
			$params = array(
				'id'=>$id,
				'op_stat'=>0,
				'op'=>''
			);

			$result = ApiUtil::api('account/feedback/op', $params);

			$this->renderJSON($result);
		}
	}
}