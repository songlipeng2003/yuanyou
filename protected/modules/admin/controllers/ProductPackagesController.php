<?php

class ProductPackagesController extends Controller
{
	public function actionCreate()
	{
		$model=new ProductPackageForm();
		if(isset($_POST['ProductPackageForm']))
		{
			$model->attributes=$_POST['ProductPackageForm'];
			if($model->validate())
			{
				$names = Yii::app()->request->getParam('name');
				$months = Yii::app()->request->getParam('month');
				$descs = Yii::app()->request->getParam('desc');
				$prices = Yii::app()->request->getParam('price');
				$products = array();
				if($names && is_array($names)){
					foreach ($names as $key => $value) {
						$products[] = array(
							'name'=>$names[$key],
							'month'=>$months[$key],
							'price'=>$prices[$key],
							'desc'=>$descs[$key]
						);
					}

				}
				$model->products = $products;
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('productPackages/index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];
		
		$this->render('create',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$result = ApiUtil::api('product_package/remove', array('id'=>$id));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex()
	{

		$this->render('index');
	}

	public function actionList()
	{
		$type = Yii::app()->request->getParam('type', '');
		$country = Yii::app()->request->getParam('country', '');
		$params = array(
			'type'=>$type,
			'country'=>$country
		);
		$result = ApiUtil::api('product_package/list', $params);

		$this->renderJSON($result);
	}

	public function actionUpdate()
	{
		$this->render('update');
	}
}