<?php

class OrdersController extends Controller
{
	public function actionCreate()
	{
		$this->render('create');
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionList()
	{
		$uid = Yii::app()->request->getParam('uid', '');
		$orderid = Yii::app()->request->getParam('orderid', '');
		$beg = Yii::app()->request->getParam('beg', '20000101000000');
		$end = Yii::app()->request->getParam('end', '20501230000000');
		$params=array(
		);

		if($beg){
	        $beg = strtotime($beg);
	        $beg = strftime('%Y%m%d%H%M%S', $beg);
	        $params['beg'] = $beg;
		}
		if($end){
	        $end = strtotime($end);
	        $end = strftime('%Y%m%d%H%M%S', $end);
	        $params['end'] = $end;
		}
		if($uid){
			$params['uid'] = $uid;
		}
		if($orderid){
			$params['orderid'] = $orderid;
		}

		$result = ApiUtil::api('order/list4admin', $params);

		$this->renderJSON($result);
	}

	public function actionStat()
	{
		$this->render('stat');
	}

	public function actionStatList()
	{
		$country = Yii::app()->request->getParam('country', '');
		$orderid = Yii::app()->request->getParam('orderid', '');
		$beg = Yii::app()->request->getParam('beg', '20000101000000');
		$end = Yii::app()->request->getParam('end', '20501230000000');
		$export = Yii::app()->request->getParam('export', false);
		$params=array(
		);

		if($beg){
	        $beg = strtotime($beg);
	        $beg = strftime('%Y%m%d%H%M%S', $beg);
	        $params['beg'] = $beg;
		}
		if($end){
	        $end = strtotime($end);
	        $end = strftime('%Y%m%d%H%M%S', $end);
	        $params['end'] = $end;
		}
		if($country){
			$params['country'] = $country;
		}

		$result = ApiUtil::api('order/stat', $params);
		if($export && $result['list']){
			$this->exportArray($result['list']);
		}

		$this->renderJSON($result);
	}
}