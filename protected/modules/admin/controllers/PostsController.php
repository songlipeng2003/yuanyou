<?php

class PostsController extends Controller
{
	public function actionCreate()
	{
		$model=new PostForm();
		if(isset($_POST['PostForm']))
		{
			$model->attributes=$_POST['PostForm'];
			if($model->validate())
			{
				$user = Yii::app()->user;
				$model->creator = $user->id;
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('posts/index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		$this->render('create',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$result = ApiUtil::api('post/delete', array('id'=>$id));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex()
	{
		$model = new PostForm();

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		$this->render('index',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionList()
	{
		$country = Yii::app()->request->getParam('country', '');
		$limit = intval(Yii::app()->request->getParam('limit', 10));
		$page = intval(Yii::app()->request->getParam('page', 1));
		$skip = ($page-1)*$limit;
		$params=array(
			'country'=>$country,
			'skip'=>$skip,
			'count'=>$limit
		);

		$result = ApiUtil::api('post/list', $params);

		$this->renderJSON($result);
	}

	public function actionUpdate($id)
	{
		$model=new PostForm();
		if(isset($_POST))
		{
			$model->attributes=$_POST;
			$model->id = $id;
			if($model->validate())
			{
				if($model->update()){
					$this->renderJSON(array(
						'code'=>0
					));
				}
				$this->renderJSON(array(
					'code'=>500
				));
			}

			$this->renderJSON(array(
				'code'=>500
			));
		}
	}
}