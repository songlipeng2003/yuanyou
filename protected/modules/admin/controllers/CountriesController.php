<?php

class CountriesController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionList()
	{
		$result = ApiUtil::api('country/list');

		if($result['code']==0){
			$countries = array();
			foreach ($result['list'] as $key => $value) {
				$countries[] = array(
					'id'=>$key,
					'name'=>$value
				);
			}
			$result['list'] = $countries;
		}

		$this->renderJSON($result);
	}

	public function actionMap()
	{
		$result = ApiUtil::api('country/list');
		$this->renderJSON($result);
	}

	public function actionDelete()
	{
		$id = Yii::app()->request->getParam('id');
		if(Yii::app()->request->isPostRequest && $id)
		{
			$result = ApiUtil::api('country/list');

			$list = $result['list'];

			if(isset($list[$id])){
				unset($list[$id]);
				$result = ApiUtil::api('country/update', array('country_list'=>json_encode($list)));
			}

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionCreate()
	{
		$model=new CountryForm();
		if(isset($_POST['CountryForm']))
		{
			$model->attributes=$_POST['CountryForm'];
			if($model->validate())
			{
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}
		$this->render('create',array('model'=>$model));
	}
}