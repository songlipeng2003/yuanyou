<?php

class SharesController extends Controller
{
	public function actionCreate()
	{
		$model=new ShareForm();
		if(isset($_POST['ShareForm']))
		{
			$model->attributes=$_POST['ShareForm'];
			if($model->validate())
			{
				$user = Yii::app()->user;
				$model->creator = $user->id;
				if($model->create()){
					Yii::app()->user->setFlash('success', '创建成功');

					$this->redirect(array('shares/index'));
				}
				Yii::app()->user->setFlash('error', '创建失败');
			}
		}

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		$this->render('create',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$result = ApiUtil::api('share/delete', array('id'=>$id));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex()
	{
		$model = new ShareForm();

		$result = ApiUtil::api('country/list');
		$countries = $result['list'];

		$this->render('index',array(
			'model'=>$model,
			'countries'=>$countries
		));
	}

	public function actionList()
	{
		// $country = Yii::app()->request->getParam('country');
		$type = Yii::app()->request->getParam('type', 0);
		$limit = intval(Yii::app()->request->getParam('limit', 10));
		$page = intval(Yii::app()->request->getParam('page', 1));
		$skip = ($page-1)*$limit;
		$beg = Yii::app()->request->getParam('beg', '20000101000000');
		$end = Yii::app()->request->getParam('end', '20501230000000');
		$params=array(
			'type'=>$type, 
			'skip'=>$skip, 
			'count'=>$limit
		);

		if($beg){
	        $beg = strtotime($beg);
	        $beg = strftime('%Y%m%d%H%M%S', $beg);
	        $params['beg'] = $beg;
		}
		if($end){
	        $end = strtotime($end);
	        $end = strftime('%Y%m%d%H%M%S', $end);
	        $params['end'] = $end;
		}

		$result = ApiUtil::api('share/list4admin', $params);

		$this->renderJSON($result);
	}

	public function actionUpdate($id)
	{
		$model=new ShareForm();
		if(isset($_POST))
		{
			$model->attributes=$_POST;
			$model->id = $id;
			if($model->validate())
			{
				if($model->update()){
					$this->renderJSON(array(
						'code'=>0
					));
				}
				$this->renderJSON(array(
					'code'=>500
				));
			}

			$this->renderJSON(array(
				'code'=>500
			));
		}
	}

	public function actionValid($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$valid = Yii::app()->request->getParam('valid', 1);
			$result = ApiUtil::api('share/valid', array('id'=>$id, 'valid'=>$valid));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionHot($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$valid = Yii::app()->request->getParam('hot', 1);
			$result = ApiUtil::api('share/hot', array('id'=>$id, 'is_hot'=>$valid));

			$this->renderJSON($result);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
}