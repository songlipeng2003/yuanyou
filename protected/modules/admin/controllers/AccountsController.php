<?php

class AccountsController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionList()
    {
        $limit = intval(Yii::app()->request->getParam('limit', 10));
        $page = intval(Yii::app()->request->getParam('page', 1));
        $sid = Yii::app()->request->getParam('sid');
        $uid = Yii::app()->request->getParam('uid');
        $skip = ($page-1)*$limit;
        $params=array(
            'skip'=>$skip, 
            'count'=>$limit,
        );
        if($sid){
            $params['sid']=$sid;
        }
        if($uid){
            $params['uid']=$uid;
        }
        if($sid || $uid){
            $result = ApiUtil::api('account/search', $params);
        }else{
            $result = ApiUtil::api('account/list', $params);
        }

        $this->renderJSON($result);
    }

    public function actionDelete()
    {
        $uid = Yii::app()->request->getParam('uid');
        $sid = Yii::app()->request->getParam('sid');

        if($uid)
        {
            $params = array('uid'=>$uid);
        }else{
            $params = array('sid'=>$sid);
        }

        if(Yii::app()->request->isPostRequest)
        {
            $result = ApiUtil::api('account/delete', $params);

            $this->renderJSON($result);
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionLog()
    {
        $uid = Yii::app()->request->getParam('uid');
        $sid = Yii::app()->request->getParam('sid');

        $limit = intval(Yii::app()->request->getParam('limit', 10));
        $page = intval(Yii::app()->request->getParam('page', 1));
        $skip = ($page-1)*$limit;
        $params=array(
            'skip'=>$skip, 
            'count'=>$limit,
        );
        if($uid)
        {
            $params['uid'] = $uid;
        }else{
            $params['sid'] = $sid;
        }
        $result = ApiUtil::api('account/log', $params);

        $this->renderJSON($result);
    }

    public function actionLock()
    {
        $uid = Yii::app()->request->getParam('uid');
        $sid = Yii::app()->request->getParam('sid');

        if($uid)
        {
            $params = array('uid'=>$uid);
        }else{
            $params = array('sid'=>$sid);
        }
        $result = ApiUtil::api('account/lock', $params);

        $this->renderJSON($result);
    }

    public function actionUnLock()
    {
        $uid = Yii::app()->request->getParam('uid');
        $sid = Yii::app()->request->getParam('sid');

        if($uid)
        {
            $params = array('uid'=>$uid);
        }else{
            $params = array('sid'=>$sid);
        }
        $result = ApiUtil::api('account/unlock', $params);

        $this->renderJSON($result);
    }

}