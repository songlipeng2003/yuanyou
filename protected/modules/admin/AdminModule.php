<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			if(isset($_SERVER['CONTENT_TYPE'])){
				$content_type_args = explode(';', $_SERVER['CONTENT_TYPE']); //parse content_type string
				if ($content_type_args[0] == 'application/json')
				{
					$_POST = json_decode(file_get_contents('php://input'),true);
				}
			}
			$controller->layout='/layouts/main';

			if(Yii::app()->user->isGuest)
			{
				$controller->redirect('/site/login');
			}

			return true;
		}
		else
			return false;
	}
}
