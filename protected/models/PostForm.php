<?php

class PostForm extends CFormModel
{
    public $id;
    public $title;
    public $country;
    public $content;
    public $creator;
    public $image;
    public $pic_url;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('title, country, content', 'required'),
            array('image, pic_url', 'safe')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'title'=>'标题',
            'country'=>'国家',
            'content'=>'内容',
            'image'=>'图片',
            'pic_url'=>'图片',
            'creator'=>'创建者'
        );
    }

    public function create()
    {
        $params = $this->attributes;
        if($params['image']){
            $params['pic_url'] = implode(';', $params['image']);
        }
        unset($params['image']);
        $result = ApiUtil::api('post/create', $params);
        if($result['code']==0){
            $this->id = $result['id'];
            return true;
        }

        return false;
    }

    public function update()
    {
        $params = $this->attributes;
        unset($params['create_at']);
        $result = ApiUtil::api('post/modify', $params);
        if($result['code']==0){
            return true;
        }

        return false;
    }
}