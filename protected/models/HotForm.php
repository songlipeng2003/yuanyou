<?php

class HotForm extends CFormModel
{
    public $id;
    public $country;
    public $pos;
    public $en_name;
    public $cn_name;
    public $pic;
    public $content;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('country, pos, content, en_name, cn_name, pic', 'required')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'country'=>'国家',
            'pos'=>'未知经纬度',
            'en_name'=>'英文名字',
            'cn_name'=>'中文名字',
            'pic'=>'图片',
            'content'=>'内容',
        );
    }

    public function create()
    {
        $result = ApiUtil::api('hot/create', $this->attributes);
        if($result['code']==0){
            $this->id = $result['id'];
            return true;
        }

        return false;
    }
}