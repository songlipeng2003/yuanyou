<?php

class ProductForm extends CFormModel
{
    public $id;
    public $name;
    public $desc;
    public $price;
    public $stat;
    public $country;
    public $inner_prop;
    public $buy_limit;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('name, price, country, desc', 'required'),
            array('price', 'numerical'),
            array('buy_limit', 'safe')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'name'=>'商品名称',
            'desc'=>'商品描述',
            'price'=>'价格',
            'stat'=>'状态',
            'country'=>'国家',
            'inner_prop'=>'商品内在属性',
            'buy_limit'=>'购买数量限制'
        );
    }

    public function create()
    {
        $attributes = $this->attributes;
        unset($attributes['id']);
        $result = ApiUtil::api('product/create', $attributes);
        if($result['code']==0){
            $this->id = $result['id'];
            return true;
        }

        return false;
    }

    public function update()
    {
        $result = ApiUtil::api('product/modify', $this->attributes);
        if($result['code']==0){
            return true;
        }

        return false;
    }
}