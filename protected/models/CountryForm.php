<?php

class CountryForm extends CFormModel
{
    public $key;
    public $value;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('key, value', 'required')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'key'=>'英文名',
            'value'=>'中文名'
        );
    }

    public function create()
    {
        $result = ApiUtil::api('country/list');

        $list = $result['list'];

        if(!isset($list[$this->key])){
            $list[$this->key] = $this->value;
            $result = ApiUtil::api('country/update', array('country_list'=>json_encode($list)));

            return true;
        }

        return false;
    }
}