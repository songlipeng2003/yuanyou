<?php

class ShareForm extends CFormModel
{
    public $id;
    public $name;
    public $country;
    public $pic;
    public $content;
    public $is_hot=false;
    public $creator;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('name, country, pic, content', 'required'),
            array('is_hot', 'boolean')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'name'=>'名称',
            'country'=>'国家',
            'pic'=>'图片',
            'content'=>'内容',
            'is_hot'=>'是否精华',
            'creator'=>'创建者'
        );
    }

    public function create()
    {
        $result = ApiUtil::api('share/create', $this->attributes);
        if($result['code']==0){
            $this->id = $result['id'];
            return true;
        }

        return false;
    }

    public function update()
    {
        $result = ApiUtil::api('share/modify', $this->attributes);
        if($result['code']==0){
            return true;
        }

        return false;
    }
}