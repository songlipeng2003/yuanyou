<?php

class AdminForm extends CFormModel
{
    public $id;
    public $username;
    public $pwd;
    public $confirm_pwd;
    public $level;
    public $country;
    public $country_list;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('username, level, country', 'required'),
            array('pwd, confirm_pwd', 'required', 'on'=>'create'),
            array('pwd', 'safe'),
            array('confirm_pwd', 'compare', 'compareAttribute'=>'pwd')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'username'=>'用户名',
            'pwd'=>'密码',
            'confirm_pwd'=>'确认密码',
            'level'=>'等级',
            'country'=>'可管理的国家',
            'country_list'=>'可管理的国家列表',
        );
    }

    public function create()
    {
        $this->country_list = implode(',', $this->country);
        $params = $this->attributes;
        unset($params['country']);
        unset($params['id']);
        $result = ApiUtil::api('admin/create', $params);

        if($result['code']==0){
            return true;
        }

        return false;
    }

    public function update()
    {
        $this->country_list = implode(',', $this->country);
        $params = $this->attributes;
        $data['username'] = $params['username'];
        $data['level'] = $params['level'];
        if($params['pwd']){
            $data['pwd'] = $params['pwd'];
        }
        $data['country_list'] = implode(',', $params['country']);
        $result = ApiUtil::api('admin/modify', $data);

        if($result['code']==0){
            return true;
        }

        return false;
    }
}