<?php

class ProductPackageForm extends CFormModel
{
    public $id;
    public $name;
    public $type;
    public $country;
    public $products;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('name, type, country', 'required')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'name'=>'列表名称',
            'type'=>'标签',
            'country'=>'国家',
        );
    }

    public function create()
    {
        $result = ApiUtil::api('product_package/add', array(
            'name'=>$this->name,
            'type'=>$this->type,
            'country'=>$this->country
        ));
        $productPackageId = $result['id'];
        if($result['code']==0){
            $this->id = $result['id'];

            foreach ($this->products as $product) {
                $product['country'] = $this->country;
                $product['stat'] = 1;
                $product['inner_prop'] = array(
                    'type'=>'map',
                    'month'=>$product['month'],
                    'appid'=>'',
                    'ios_price'=>''
                );
                $product['buy_limit'] = 999;
                $product['inner_prop'] = json_encode($product['inner_prop']);

                $result = ApiUtil::api('product/add', $product);
                $productId = $result['id'];

                $result = ApiUtil::api('product_package/add_product', array(
                    'package_id'=>$productPackageId,
                    'product_id'=>$productId
                ));
            }

            return true;
        }

        return false;
    }
}