<?php

class AdForm extends CFormModel
{
    public $id;
    public $title;
    public $expire_at;
    public $content;
    public $link;
    public $where;
    public $customer_name;
    public $creator;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('title, expire_at, link, content', 'required'),
            array('link', 'url'),
            array('where', 'safe')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'title'=>'标题',
            'expire_at'=>'过期日期',
            'content'=>'内容',
            'link'=>'链接地址',
            'where'=>'广告位置',
            'customer_name'=>'客户名称',
            'creator'=>'创建者'
        );
    }

    public function create()
    {
        $params = $this->attributes;
        unset($params['id']);
        $expire_at = strtotime($params['expire_at']);
        $expire_at = strftime('%Y%m%d%H%M%S', $expire_at);
        $params['expire_at'] = $expire_at;

        $result = ApiUtil::api('ad/create', $params);
        if($result['code']==0){
            $this->id = $result['id'];
            return true;
        }

        return false;
    }

    public function update()
    {
        $params = $this->attributes;
        $expire_at = strtotime($params['expire_at']);
        $expire_at = strftime('%Y%m%d%H%M%S', $expire_at);
        $params['expire_at'] = $expire_at;
        
        $result = ApiUtil::api('ad/modify', $params);
        if($result['code']==0){
            return true;
        }

        return false;
    }
}