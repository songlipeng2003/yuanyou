<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>


<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login.css" />

<div class="login-bg">
	<img src="/images/login_bg.png"/>
</div>

<div class="form clearfix span11">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

	<div class="span8">
		<?php echo $form->errorSummary($model); ?>

	    <?php 
	        echo $form->textFieldRow($model, 'username');
	        echo $form->passwordFieldRow($model, 'password');
	        echo $form->checkBoxRow($model, 'rememberMe');
	    ?>
	</div>

	<div class="buttons span2">
		<?php echo CHtml::submitButton('登录'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
