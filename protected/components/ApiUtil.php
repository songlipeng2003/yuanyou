<?php
class ApiUtil {
    const BASE_API_URL = 'http://115.28.56.233:8001';
    const BASE_URL = 'http://115.28.56.233:8001';

    public static function post($url, $params=array()){
        $curl = curl_init();

        $header[] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 10";
        $header[] = "Accept-Charset: GB2312,ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: zh-cn,zh,en-us,en;q=0.5";

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($curl);

        $i=0;
        while((!$result)&&$i<3){
            sleep($i);
            $result = curl_exec($curl);
            $i++;
        }

        curl_close($curl);

        return $result;
    }

    public static function api($url, $params=array()){
        $url = self::BASE_API_URL . '/' . $url;

        Yii::trace($url . json_encode($params), "api");

        $result = self::post($url, $params);

        Yii::trace($result, "api" );

        if($result){
            $result = json_decode($result, true);
        }

        return $result;
    }
}
