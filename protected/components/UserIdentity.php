<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{

		$params = array(
			'username'=>$this->username,
			'pwd'=>$this->password
		);

		$result = ApiUtil::api('superadmin/login', $params);

		if($result && $result['code']==0){
			$this->errorCode = UserIdentity::ERROR_NONE;
			Yii::app()->user->setState('user_level', 1);
			return true;
		}

		$result = ApiUtil::api('admin/login', $params);
		if($result && $result['code']==0){
			$this->errorCode = UserIdentity::ERROR_NONE;
			Yii::app()->user->setState('user_level', $result['info']['level']);
			return true;
		}

		$this->errorCode = UserIdentity::ERROR_PASSWORD_INVALID;
		return false;
	}
}