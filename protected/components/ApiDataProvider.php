<?php
class ApiDataProvider extends CDataProvider{
    private $_response;
    private $_url;
    private $_params;
    public $rawData=array();

    public function __construct($url, $params, $config=array()){
        $this->_url = $url;
        $this->_params = $params;
        foreach($config as $key=>$value){
            $this->$key=$value;
        }
    }

    protected function fetchData(){
        if(!$this->_response){
            $pagination=$this->getPagination();
            $params = array(
                'skip'=>0,
                'count'=>1
            );

            $params = array_merge($this->_params, $params);

            $this->_response = ApiUtil::api($this->_url, $params);

            if(($pagination=$this->getPagination())!==false){
                $pagination->setItemCount($this->getTotalItemCount());
            }

            $params = array(
                'skip'=>$pagination->getOffset(),
                'count'=>$pagination->getLimit()
            );

            $params = array_merge($this->_params, $params);
            $this->_response = ApiUtil::api($this->_url, $params);

            $this->rawData = $this->_response['list'];

            return $this->rawData;
        }
    }

    protected function fetchKeys(){
        if(!$this->_response){
            $this->fetchData();
        }
        $keys=array();
        foreach($this->getData() as $i=>$data){
            $keys[$i]=is_object($data) ? $data->id : $data['id'];
        }
        return $keys;
    }

    protected function calculateTotalItemCount(){
        if(!$this->_response){
            $this->fetchData();
        }
        return $this->_response['count'];
    }
}