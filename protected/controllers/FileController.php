<?php

class FileController extends Controller
{
    public function actionUpload()
    {
        $result = array('code'=>1);
        if (!empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];

            $targetPath = Yii::getPathOfAlias('webroot') . '/upload/tmp/';
            $newFileName = md5(uniqid());
            $ext = pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);
            $newFileName = $newFileName . '.' . $ext;
            $targetFile =  $targetPath . $newFileName;

            @ mkdir($targetPath, 0755, true);
            move_uploaded_file($tempFile, $targetFile);

            $params = array(
                'img'=>'@'.$targetFile.';filename='.$newFileName.';type='.$_FILES['Filedata']['type']
            );

            $result = ApiUtil::api('resource/image/upload', $params);
            if($result && $result['code']==0){
                $result['url'] = ApiUtil::BASE_URL.$result['path'];
            }
        }

        $this->renderJSON($result);
    }
}