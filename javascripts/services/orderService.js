"use strict";
angular.module('myApp').
    factory('OrderService',['$http',function($http){
        return {
            list: function(filter, page, limit){
                var url = '/admin/orders/list?page='+page+'&limit='+limit;
                if(filter.uid!='' && filter.uid!=null){
                    url += '&uid='+filter.uid;
                }
                if(filter.orderid!='' && filter.orderid!=null){
                    url += '&orderid='+filter.orderid;
                }
                if(filter.beg!='' && filter.beg!=null){
                    url += '&beg='+filter.beg;
                }
                if(filter.end!='' && filter.end!=null){
                    url += '&end='+filter.end;
                }
                return $http.get(url);
            },
            statList: function(filter){
                var url = '/admin/orders/statList?0=0';
                if(filter.country!='' && filter.country!=null){
                    url += '&country='+filter.country;
                }
                if(filter.beg!='' && filter.beg!=null){
                    url += '&beg='+filter.beg;
                }
                if(filter.end!='' && filter.end!=null){
                    url += '&end='+filter.end;
                }
                if(filter.export!='' && filter.export!=null){
                    url += '&export='+filter.export;
                }
                return $http.get(url);
            }
        };
    }]);