"use strict";
angular.module('myApp').
    factory('AdminService',['$http',function($http){
        return {
            list: function(){
                return $http.get('/admin/admins/list');
            },
            update: function(admin){
                return $http.post('/admin/admins/update/'+admin.id, admin);
            },
            remove: function(admin){
                return $http.post('/admin/admins/delete?username='+admin.username);
            }
        };
    }]);