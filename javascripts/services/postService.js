"use strict";
angular.module('myApp').
    factory('PostService',['$http',function($http){
        return {
            list: function(filter, page, limit){
                var url = '/admin/posts/list?page='+page+'&limit='+limit;
                if(filter.country!='' && filter.country!=null){
                    url += '&country='+filter.country;
                }
                return $http.get(url);
            },
            update: function(post){
                return $http.post('/admin/posts/update/'+post.id, post);
            },
            remove: function(post){
                return $http.post('/admin/posts/delete/'+post.id);
            }
        };
    }]);