"use strict";
angular.module('myApp').
    factory('ShareService',['$http',function($http){
        return {
            list: function(filter, page, limit){
                var url = '/admin/shares/list?page='+page+'&limit='+limit;
                if(filter.country!='' && filter.country!=null){
                    url += '&country='+filter.country;
                }
                url += '&type='+filter.type;
                if(filter.beg!='' && filter.beg!=null){
                    url += '&beg='+filter.beg;
                }
                if(filter.end!='' && filter.end!=null){
                    url += '&end='+filter.end;
                }
                return $http.get(url);
            },
            update: function(share){
                return $http.post('/admin/shares/update/'+share.id, share);
            },
            remove: function(share){
                return $http.post('/admin/shares/delete/'+share.id);
            },
            valid: function(share){
                return $http.post('/admin/shares/valid/'+share.id);
            },
            set_hot: function(share, hot){
                return $http.post('/admin/shares/hot/'+share.id+'?hot='+hot);
            }
        };
    }]);