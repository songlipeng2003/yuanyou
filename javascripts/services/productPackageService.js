"use strict";
angular.module('myApp').
    factory('ProductPackageService',['$http',function($http){
        return {
            list: function(filter, page, limit){
                var url = '/admin/productPackages/list?page='+page+'&limit='+limit;
                if(filter.type!='' && filter.type!=null){
                    url += '&type='+filter.type;
                }
                if(filter.country!='' && filter.country!=null){
                    url += '&country='+filter.country;
                }
                return $http.get(url);
            },
            update: function(product){
                return $http.post('/admin/productPackages/update/'+product.id, product);
            },
            remove: function(product){
                return $http.post('/admin/productPackages/delete/'+product.id);
            }
        };
    }]);