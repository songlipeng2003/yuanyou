"use strict";
angular.module('myApp').
    factory('CountryService',['$http',function($http){
        return {
            list: function(){
                return $http.get('/admin/countries/list');
            },
            map: function(){
                return $http.get('/admin/countries/map');
            },
            remove: function(country){
                return $http.post('/admin/countries/delete?id='+country.id);
            }
        };
    }]);