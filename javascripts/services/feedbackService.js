"use strict";
angular.module('myApp').
    factory('FeedbackService',['$http',function($http){
        return {
            list: function(filter, page, limit){
                var url = '/admin/feedbacks/list?page='+page+'&limit='+limit;
                if(filter.op!='' && filter.op!=null){
                    url += '&op='+filter.op;
                }
                if(filter.beg!='' && filter.beg!=null){
                    url += '&beg='+filter.beg;
                }
                if(filter.end!='' && filter.end!=null){
                    url += '&end='+filter.end;
                }
                return $http.get(url);
            },
            update: function(feedback){
                return $http.post('/admin/feedbacks/update/'+feedback.id, feedback);
            },
            remove: function(feedback){
                return $http.post('/admin/feedbacks/delete/'+feedback.id);
            }
        };
    }]);