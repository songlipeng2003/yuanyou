"use strict";
angular.module('myApp').
    factory('ProductService',['$http',function($http){
        return {
            list: function(filter, page, limit){
                var url = '/admin/products/list?page='+page+'&limit='+limit;
                if(filter.stat!='' && filter.stat!=null){
                    url += '&stat='+filter.stat;
                }
                return $http.get(url);
            },
            update: function(product){
                return $http.post('/admin/products/update/'+product.id, product);
            },
            remove: function(product){
                return $http.post('/admin/products/delete/'+product.id);
            }
        };
    }]);