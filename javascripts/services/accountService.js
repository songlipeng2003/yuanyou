"use strict";
angular.module('myApp').
    factory('AccountService',['$http',function($http){
        return {
            list: function(filter, limit, page){
                var url = '/admin/accounts/list?page='+page+'&limit='+limit;
                if(filter.sid!='' && filter.sid!=null){
                    url += '&sid='+filter.sid;
                }
                if(filter.uid!='' && filter.uid!=null){
                    url += '&uid='+filter.uid;
                }
                return $http.get(url);
            },
            log: function(account){
                return $http.post('/admin/accounts/log?uid='+account.uid+'&sid='+account.sid);
            },
            remove: function(account){
                return $http.post('/admin/accounts/delete?uid='+account.uid+'&sid='+account.sid);
            },
            lock: function(account){
                return $http.post('/admin/accounts/lock?uid='+account.uid+'&sid='+account.sid);
            },
            unlock: function(account){
                return $http.post('/admin/accounts/unlock?uid='+account.uid+'&sid='+account.sid);
            }
        };
    }]);