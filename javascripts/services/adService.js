"use strict";
angular.module('myApp').
    factory('AdService',['$http',function($http){
        return {
            list: function(page, limit){
                return $http.get('/admin/ads/list?page='+page+'&limit='+limit);
            },
            update: function(ad){
                return $http.post('/admin/ads/update/'+ad.id, ad);
            },
            remove: function(ad){
                return $http.post('/admin/ads/delete/'+ad.id);
            }
        };
    }]);