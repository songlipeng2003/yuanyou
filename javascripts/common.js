$(function(){
    $('[data-href]').click(function(){
        location.href = $(this).data('href');
        return false;
    });
});