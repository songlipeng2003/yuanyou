"use strict";
angular.module('myApp', ['ngGrid'])
    .filter('split', function() {
        return function(input, delimiter) {
            var delimiter = delimiter || ',';

            if(input){
                return input.split(delimiter);
            }

            return {};
        } 
    })
    .controller('accountsController', function(AccountService, $scope){
        var self = this;

        $scope.is_locks = {
            false: '正常',
            true: '关闭'
        };

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 20, //Size of Paging data
            currentPage: 1 //what page they are currently on
        };

        $scope.filter = {
            sid: '',
            uid: ''
        }

        $scope.myDefs = [
            {field: 'sid', displayName: '编号', width: 100},
            {field: 'uid', displayName: '用户名'},
            {field: 'last_login_dev', displayName: '设备码'},
            {field: 'nick', displayName: '昵称', width: 80, visible:false},
            {field: 'email', displayName: 'Email', width: 80, visible:false},
            {field: 'phone_no', displayName: '手机号', width: 80, visible:false},
            {field: 'create_at', displayName: '创建时间', width: 160},
            {field: 'operation', displayName: '操作', width: '80', sortable:false, cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'+
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            columnDefs: $scope.myDefs,
            pagingOptions: $scope.pagingOptions,
            totalServerItems: 'totalServerItems',
            primaryKey: 'id',
            enablePaging: true,
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showColumnMenu: true,
            enableColumnResize: true,
            showFooter: true
        };

        self.getPagedDataAsync = function(filter, limit, page) {
            AccountService.list(filter, limit, page).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(account){
            $scope.account = account;
            AccountService.log(account).success(function(result){
                if(result.code==0){
                    $scope.logs = result.list;
                }

                $('#account_modal').modal('show');
            });
        }

        $scope.edit = function(account){
            $scope.account = account;
            $('#account_edit_modal').modal('show');
        }

        $scope.lock = function(account){
            AccountService.lock(account).success(function(result){
                if(result.code==0){
                    account.is_lock = true;
                }else{
                    alert('关闭失败');
                }
            });
            return false;    
        }

        $scope.unlock = function(account){
            AccountService.unlock(account).success(function(result){
                if(result.code==0){
                    account.is_lock = false;
                }else{
                    alert('激活失败');
                }
            });
            return false;    
        }

        $scope.remove = function(country){
            if(confirm('你确认删除吗？')){
                AccountService.remove(country).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                });
            }
        };
    });