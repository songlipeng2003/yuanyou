 "use strict";
angular.module('myApp', ['ngGrid']).
    controller('feedbacksController', function(FeedbackService, $scope){
        var self = this;

        $scope.filter = {
            beg: '',
            end: '',
            op:1
        };

        $scope.stats = {
            0: '未处理',
            1: '已处理'
        }

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 20, //Size of Paging data
            currentPage: 1 //what page they are currently on
        };

        $scope.myDefs = [
            {field: 'id', displayName: '编号', width: '50'},
            {field: 'username', displayName: '用户名', width: '200'},
            {field: 'devinfo', displayName: '设备信息'},
            {field: 'op_stat', displayName: '状态', width: '60',
                cellTemplate: '<div class="ngCellText">{{stats[row.entity[col.field]]}}</div>'},
            {field: 'create_at', displayName: '反馈时间', width: '150'},
            {field: 'op', displayName: '处理结果', visible:false},
            {field: 'sw_ver', displayName: '软件版本', visible:false},
            {field: 'data_ver', displayName: '数据版本', visible:false},
            {field: 'connect', displayName: '联系方式', visible:false},
            {field: 'operation', displayName: '操作', width: '80', cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'+
                '<a class="update" title="编辑处理" ng-show="row.entity.op_stat==0" data-toggle="tooltip" ng-click="edit(row.entity)"><i class="icon-pencil"></i></a>'+
                '<a class="delete" title="删除处理" ng-show="row.entity.op_stat==1" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            pagingOptions: $scope.pagingOptions,
            columnDefs: $scope.myDefs,
            totalServerItems: $scope.totalServerItems,
            enablePaging: true,
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showColumnMenu: true,
            enableColumnResize: true,
            enableColumnReordering: true,
            showFooter: true,
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function (filter, pageSize, page) {
            FeedbackService.list(filter, page, pageSize).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            }
            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(feedback){
            $scope.feedback = feedback;
            $('#feedback_modal').modal('show');
        }

        $scope.edit = function(feedback){
            $scope.feedback = feedback;
            $('#feedback_edit_modal').modal('show');
        }

        $scope.update = function(feedback){
            FeedbackService.update(feedback).success(function(result){
                if(result.code==0){
                    self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    $('#feedback_edit_modal').modal('hide');
                }else{
                    alert('处理失败');
                }
            });            
        }

        $scope.remove = function(feedback){
            if(confirm('你确认删除处理吗？')){
                FeedbackService.remove(feedback).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                });
            }
        }

        $scope.export = function(){
            var filter = $scope.filter;
            var page = $scope.pagingOptions.currentPage;
            var limit = $scope.pagingOptions.pageSize;
            var url = '/admin/feedbacks/list?page='+page+'&limit='+limit;
            if(filter.op!='' && filter.op!=null){
                url += '&op='+filter.op;
            }
            if(filter.beg!='' && filter.beg!=null){
                url += '&beg='+filter.beg;
            }
            if(filter.end!='' && filter.end!=null){
                url += '&end='+filter.end;
            }
            url += '&export='+1;

            location.href = url;
        }
    });