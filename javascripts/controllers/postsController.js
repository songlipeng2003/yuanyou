"use strict";
angular.module('myApp', ['ngGrid'])
    .filter('split', function() {
        return function(input, delimiter) {
            var delimiter = delimiter || ',';

            if(input){
                return input.split(delimiter);
            }

            return {};
        } 
    })
    .controller('postsController', function(PostService, CountryService, $scope){
        var self = this;

        $scope.countries = {};

        $scope.filter = {
            country: ''
        };

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 20, //Size of Paging data
            totalServerItems: 0, //how many items are on the server (for paging)
            currentPage: 1 //what page they are currently on
        };
        
        $scope.myDefs = [
            {field: 'id', displayName: '编号', width: '50'},
            {field: 'title', displayName: '标题', width: '280'},
            {field: 'country', displayName: '国家', cellTemplate: '<div class="ngCellText">{{countries[row.entity[col.field]]}}</div>'},
            {field: 'create_at', displayName: '发布时间'},
            {field: 'creator', displayName: '发布者', visible:false},
            {field: 'pic_url', displayName: '图片url', visible:false},
            {field: 'operation', displayName: '操作', width: '80', cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'+
                '<a class="update" title="修改" data-toggle="tooltip" ng-click="edit(row.entity)"><i class="icon-pencil"></i></a>'+
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            pagingOptions: $scope.pagingOptions,
            enablePaging: true,
            totalServerItems: 'totalServerItems',
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showGroupPanel: true,
            columnDefs: 'myDefs',
            showColumnMenu: true,
            enableColumnReordering: true,
            enableColumnResize:true,
            showFooter: true,
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function (filter, pageSize, page) {
            PostService.list(filter, page, pageSize).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        CountryService.map().success(function(result){
            if(result.code==0){
                $scope.countries = result.list;
            }

            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        });

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(post){
            $scope.post = post;
            $('#post_modal').modal('show');
        }

        $scope.edit = function(post){
            $scope.post = post;
            $('#images li:not([ng-repeat])').remove();
            $('#post_edit_modal').modal('show');
        }

        $scope.update = function(post){
            var images = [];
            $('input[name="PostForm[image][]"]').each(function(){
                images.push($(this).val()); 
            });
            post.pic_url = images.join(';');
            PostService.update(post).success(function(result){
                if(result.code==0){
                    self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    $('#post_edit_modal').modal('hide');
                }else{
                    alert('修改失败');
                }
            });
        }

        $scope.remove = function(post){
            if(confirm('你确认删除吗？')){
                PostService.remove(post).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                });
            }
        }
    });