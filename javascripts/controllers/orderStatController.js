"use strict";
angular.module('myApp', ['ngGrid']).
    controller('orderStatController', function(CountryService, OrderService, $scope){
        var self = this;

        $scope.filter = {
            beg: '',
            end: '',
            country: ''
        };

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 20, //Size of Paging data
            currentPage: 1 //what page they are currently on
        };

        $scope.myDefs = [
            {field: 'id', displayName: '商品号', width: '120'},
            {field: 'name', displayName: '名称'},
            {field: 'country', displayName: '国家', cellTemplate: '<div class="ngCellText">{{countries[row.entity[col.field]]}}</div>'},
            {field: 'price', displayName: '价格', width: '100'},
            {field: 'count', displayName: '数量', width: '100'}
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            columnDefs: 'myDefs',
            // pagingOptions: $scope.pagingOptions,
            // enablePaging: true,
            totalServerItems: 'totalServerItems',
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showGroupPanel: true,
            showColumnMenu: true,
            enableColumnReordering: true,
            enableColumnResize:true,
            showFooter: true,
            filterOptions: {filterText:'', useExternalFilter: false},
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function (filter) {
            OrderService.statList(filter).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        CountryService.map().success(function(result){
            if(result.code==0){
                $scope.countries = result.list;
            }

            self.getPagedDataAsync($scope.filter);
        });

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.filter);
        }, true);

        $scope.export = function(){
            var filter = $scope.filter;
            var url = '/admin/orders/statList?0=0';
            if(filter.country!='' && filter.country!=null){
                url += '&country='+filter.country;
            }
            if(filter.beg!='' && filter.beg!=null){
                url += '&beg='+filter.beg;
            }
            if(filter.end!='' && filter.end!=null){
                url += '&end='+filter.end;
            }
            url += '&export='+1;

            location.href = url;
        }
    });