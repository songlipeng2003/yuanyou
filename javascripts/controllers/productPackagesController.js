"use strict";
angular.module('myApp', ['ngGrid']).
    controller('productPackagesController', function(ProductPackageService, CountryService, $scope){
        var self = this;

        $scope.filter = {
            type: '',
            country: ''
        };

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 10, //Size of Paging data
            totalServerItems: 0, //how many items are on the server (for paging)
            currentPage: 1 //what page they are currently on
        };

        $scope.myDefs = [
            {field: 'id', displayName: '编号', width: '50'},
            {field: 'name', displayName: '列表名称', width: '280'},
            {field: 'type', displayName: '标签'},
            {field: 'country', displayName: '国家', cellTemplate: '<div class="ngCellText">{{countries[row.entity[col.field]]}}</div>'},
            {field: 'operation', displayName: '操作', width: '80', cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'+
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            // pagingOptions: $scope.pagingOptions,
            // enablePaging: true,
            totalServerItems: 'totalServerItems',
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showGroupPanel: true,
            columnDefs: 'myDefs',
            showColumnMenu: true,
            enableColumnReordering: true,
            enableColumnResize:true,
            showFooter: true,
            showFilter: true,
            filterOptions: {filterText:'', useExternalFilter: false},
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function (filter, pageSize, page) {
            ProductPackageService.list(filter, page, pageSize).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        CountryService.map().success(function(result){
            if(result.code==0){
                $scope.countries = result.list;
            }

            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        });

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(product_package){
            $scope.product_package = product_package;
            $('#product_package_modal').modal('show');
        }

        $scope.remove = function(product_package){
            if(confirm('你确认删除吗？')){
                ProductPackageService.remove(product_package).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                });
            }
        }
    });