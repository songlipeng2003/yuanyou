"use strict";
angular.module('myApp', ['ngGrid'])
    .filter('split', function() {
        return function(input, delimiter) {
            var delimiter = delimiter || ',';

            if(input){
                return input.split(delimiter);
            }

            return {};
        } 
    })
    .controller('adminsController', function(AdminService, CountryService, $scope){
        var self = this;
        $scope.levels = {
            2:'二级账户',
            3:'三级账户'
        };

        $scope.myDefs = [
            {field: 'id', displayName: '编号', width: 50},
            {field: 'username', displayName: '用户名'},
            {field: 'level', displayName: '等级', width: 80,
                cellTemplate: '<div class="ngCellText">{{levels[row.entity[col.field]]}}</div>'},
            {field: 'country_list', displayName: '国家列表', visible: false, sortable:false},
            {field: 'create_at', displayName: '创建时间', width: 160},
            {field: 'operation', displayName: '操作', width: '80', sortable:false, cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'+
                '<a class="update" title="修改" data-toggle="tooltip" href="/admin/admins/update/username/{{row.entity[\'username\']}}/level/{{row.entity[\'level\']}}/country_list/{{row.entity[\'country_list\']}}"><i class="icon-pencil"></i></a>'+
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            columnDefs: $scope.myDefs,
            primaryKey: 'id',
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showColumnMenu: true,
            enableColumnResize: true,
            showFooter: true
        };

        self.getPagedDataAsync = function () {
            AdminService.list().success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                }
            });
        };

        CountryService.map().success(function(result){
            if(result.code==0){
                $scope.countries = result.list;
            }

            self.getPagedDataAsync();
        });

        $scope.show = function(admin){
            $scope.admin = admin;
            $('#admin_modal').modal('show');
        }

        $scope.edit = function(admin){
            $scope.admin = admin;
            $('#admin_edit_modal').modal('show');
        }

        $scope.update = function(admin){
            AdminService.update(admin).success(function(result){
                if(result.code==0){
                    $('#admin_edit_modal').modal('hide');
                }else{
                    alert('修改失败');
                }
            });
            return false;    
        }

        $scope.remove = function(country){
            if(confirm('你确认删除吗？')){
                AdminService.remove(country).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync();
                    }
                });
            }
        };
    });