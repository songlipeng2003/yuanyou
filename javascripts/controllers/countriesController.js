"use strict";
angular.module('myApp', ['ngGrid']).
    controller('countriesController', function(CountryService, $scope){
        var self = this;

        $scope.myDefs = [
            {field: 'id', displayName: '英文名'},
            {field: 'name', displayName: '国家名'},
            {field: 'operation', displayName: '操作', width: '80', cellTemplate: 
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showGroupPanel: true,
            columnDefs: 'myDefs',
            showFooter: true,
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function () {
            CountryService.list().success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                }
            });
        };

        self.getPagedDataAsync();

        $scope.remove = function(country){
            if(confirm('你确认删除吗？')){
                CountryService.remove(country).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync();
                    }
                });
            }
        }
    });