"use strict";
angular.module('myApp', ['ngGrid']).
    controller('sharesController', function(ShareService, CountryService, $scope){
        var self = this;

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 20, //Size of Paging data
            currentPage: 1 //what page they are currently on
        };

        $scope.filter = {
            beg: '',
            end: '',
            country: "0",
            type: "0"
        };

        $scope.valids = {
            false:'否',
            true:'是'
        }

        $scope.bools = {
            0:'否',
            1:'是'
        }

        $scope.myDefs = [
            {field: 'id', displayName: '编号', width: '50'},
            {field: 'name', displayName: '标题', width: '250'},
            {field: 'country', displayName: '国家', width: '80', cellTemplate: '<div class="ngCellText">{{countries[row.entity[col.field]]}}</div>'},
            {field: 'pic', displayName: '图片', visible:false},
            {field: 'create_at', displayName: '创建时间', width: '160'},
            {field: 'content', displayName: '内容', visible:false},
            {field: 'creator', displayName: '发布者', visible:false},
            {field: 'is_valid', displayName: '是否审核',
                cellTemplate: '<div class="ngCellText">{{valids[row.entity[col.field]]}}</div>'},
            {field: 'is_hot', displayName: '是否精华',
                cellTemplate: '<div class="ngCellText">{{bools[row.entity[col.field]]}}</div>'},
            {field: 'operation', displayName: '操作', width: '80', cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" target="_blank" href="http://115.28.56.233:8001/share/article-{{row.entity.id}}"><i class="icon-eye-open"></i></a>'+
                '<a class="update" title="修改" data-toggle="tooltip" ng-click="edit(row.entity)"><i class="icon-pencil"></i></a>'+
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            columnDefs: 'myDefs',
            pagingOptions: $scope.pagingOptions,
            enablePaging: true,
            totalServerItems: 'totalServerItems',
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showGroupPanel: true,
            showColumnMenu: true,
            enableColumnReordering: true,
            enableColumnResize:true,
            showFooter: true,
            primaryKey: 'id'
        };

        $scope.allCountries = {};

        CountryService.map().success(function(result){
            if(result.code==0){
                $scope.countries = result.list;
            }

            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        });

        self.getPagedDataAsync = function (filter, pageSize, page) {
            ShareService.list(filter, page, pageSize).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(share){
            $scope.share = share;
            $('#share_modal').modal('show');
        }

        $scope.edit = function(share){
            $scope.share = share;
            $('#share_edit_modal').modal('show');
        }

        $scope.update = function(share){
            ShareService.update(share).success(function(result){
                if(result.code==0){
                    $('#share_edit_modal').modal('hide');
                }else{
                    alert('修改失败');
                }
            });            
        }

        $scope.set_hot = function(share){
            ShareService.set_hot(share, 1).success(function(result){
                if(result.code==0){
                    share.is_hot=1;
                    $('#share_edit_modal').modal('hide');
                }else{
                    alert('修改失败');
                }
            });            
        }

        $scope.set_not_hot = function(share){
            ShareService.set_hot(share, 0).success(function(result){
                if(result.code==0){
                    share.is_hot=0;
                    $('#share_edit_modal').modal('hide');
                }else{
                    alert('修改失败');
                }
            });            
        }

        $scope.remove = function(share){
            if(confirm('你确认删除吗？')){
                ShareService.remove(share).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                    $('#share_edit_modal').modal('hide');
                });
            }
        }

        $scope.valid = function(share){
            if(confirm('你确认通过吗？')){
                ShareService.valid(share).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                    $('#share_edit_modal').modal('hide');
                });
            }
        }
    });