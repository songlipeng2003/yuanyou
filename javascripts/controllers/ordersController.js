18505286818 
"use strict";
angular.module('myApp', ['ngGrid']).
    controller('ordersController', function(OrderService, $scope){
        var self = this;

        $scope.filter = {
            beg: '',
            end: '',
            uid: '',
            orderid: ''
        };

        $scope.stats = {
            0: '未支付',
            1: '已支付',
            2: '已使用'
        }

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 20, //Size of Paging data
            currentPage: 1 //what page they are currently on
        };

        $scope.myDefs = [
            {field: 'id', displayName: '订单号', width: '120'},
            {field: 'user.uid', displayName: '用户名'},
            {field: 'product.name', displayName: '商品名称'},
            {field: 'create_at', displayName: '创建时间'},
            {field: 'pay_at', displayName: '支付时间'},
            {field: 'stat', displayName: '订单状态',
                cellTemplate: '<div class="ngCellText">{{stats[row.entity[col.field]]}}</div>'},
            {field: 'price', displayName: '单价', width: '80', visible:false},
            {field: 'count', displayName: '数量', width: '50', visible:false},
            {field: 'desc', displayName: '订单描述', visible: false},
            {field: 'operation', displayName: '操作', width: '80', cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            columnDefs: 'myDefs',
            pagingOptions: $scope.pagingOptions,
            enablePaging: true,
            totalServerItems: 'totalServerItems',
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showGroupPanel: true,
            showColumnMenu: true,
            enableColumnReordering: true,
            enableColumnResize:true,
            showFooter: true,
            filterOptions: {filterText:'', useExternalFilter: false},
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function (filter, pageSize, page) {
            OrderService.list(filter, page, pageSize).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(order){
            $scope.order = order;
            $('#order_modal').modal('show');
        }
    });