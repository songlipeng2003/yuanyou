"use strict";
angular.module('myApp', ['ngGrid']).
    controller('productsController', function(ProductService, CountryService, $scope){
        var self = this;

        $scope.filter = {
            stat: 0
        };

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 10, //Size of Paging data
            totalServerItems: 0, //how many items are on the server (for paging)
            currentPage: 1 //what page they are currently on
        };

        $scope.myDefs = [
            {field: 'id', displayName: '编号', width: '50'},
            {field: 'name', displayName: '商品名称', width: '280'},
            {field: 'price', displayName: '价格'},
            {field: 'stat', displayName: '商品状态'},
            {field: 'country', displayName: '国家', cellTemplate: '<div class="ngCellText">{{countries[row.entity[col.field]]}}</div>'},
            {field: 'buy_limit', displayName: '购买数量限制', visible:false},
            {field: 'operation', displayName: '操作', width: '80', cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'+
                '<a class="update" title="修改" data-toggle="tooltip" ng-click="edit(row.entity)"><i class="icon-pencil"></i></a>'+
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            pagingOptions: $scope.pagingOptions,
            enablePaging: true,
            totalServerItems: 'totalServerItems',
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showGroupPanel: true,
            columnDefs: 'myDefs',
            showColumnMenu: true,
            enableColumnReordering: true,
            enableColumnResize:true,
            showFooter: true,
            showFilter: true,
            filterOptions: {filterText:'', useExternalFilter: false},
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function (filter, pageSize, page) {
            ProductService.list(filter, page, pageSize).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        CountryService.map().success(function(result){
            if(result.code==0){
                $scope.countries = result.list;
            }

            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        });

        $scope.$watch('[filter, pagingOptions]', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(product){
            $scope.product = product;
            $('#product_modal').modal('show');
        }

        $scope.edit = function(product){
            $scope.product = product;
            $('#product_edit_modal').modal('show');
        }

        $scope.update = function(product){
            ProductService.update(product).success(function(result){
                if(result.code==0){
                    $('#product_edit_modal').modal('hide');
                }else{
                    alert('修改失败');
                }
            });            
        }

        $scope.remove = function(product){
            if(confirm('你确认删除吗？')){
                ProductService.remove(product).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.filter, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                });
            }
        }
    });