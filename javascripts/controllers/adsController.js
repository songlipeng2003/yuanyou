 "use strict";
angular.module('myApp', ['ngGrid']).
    controller('adsController', function(AdService, $scope){
        var self = this;

        $scope.pagingOptions = {
            pageSizes: [10, 20], //page Sizes
            pageSize: 10, //Size of Paging data
            currentPage: 1 //what page they are currently on
        };

        $scope.myDefs = [
            {field: 'id', displayName: '编号', width: '50'},
            {field: 'title', displayName: '标题', width: '280'},
            {field: 'expire_at', displayName: '过期时间'},
            {field: 'create_at', displayName: '创建时间', visible:false},
            {field: 'content', displayName: '广告内容', visible:false},
            {field: 'link', displayName: '链接', visible:false},
            {field: 'where', displayName: '位置', visible:false},
            {field: 'customer_name', displayName: '客户名'},
            {field: 'creator', displayName: '广告发布者', visible:false},
            {field: 'operation', displayName: '操作', width: '80', sortable:false, cellTemplate: 
                '<a class="view" title="查看" data-toggle="tooltip" ng-click="show(row.entity)"><i class="icon-eye-open"></i></a>'+
                '<a class="update" title="修改" data-toggle="tooltip" ng-click="edit(row.entity)"><i class="icon-pencil"></i></a>'+
                '<a class="delete" title="删除" data-toggle="tooltip" ng-click="remove(row.entity)"><i class="icon-trash"></i></a>'
            }
        ];

        $scope.myData = [];

        $scope.gridOptions = {
            i18n: 'zh-cn',
            data: 'myData',
            selectedItems: $scope.selections,
            pagingOptions: $scope.pagingOptions,
            columnDefs: $scope.myDefs,
            totalServerItems: $scope.totalServerItems,
            enablePaging: true,
            enableRowSelection: true,
            multiSelect: false,
            enableRowReordering: false,
            showColumnMenu: true,
            enableColumnResize: true,
            enableColumnReordering: true,
            showFooter: true,
            primaryKey: 'id'
        };

        self.getPagedDataAsync = function (pageSize, page) {
            AdService.list(page, pageSize).success(function(result){
                if(result.code==0){
                    $scope.myData = result.list;
                    $scope.totalServerItems = result.count;
                }
            });
        };

        self.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function () {
            if (!self.poInit || self.gettingData) {
                self.poInit = true;
                return;
            } 
            self.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }, true);

        $scope.show = function(ad){
            $scope.ad = ad;
            $('#ad_modal').modal('show');
        }

        $scope.edit = function(ad){
            $scope.ad = ad;
            $('#ad_edit_modal').modal('show');
        }

        $scope.update = function(ad){
            AdService.update(ad).success(function(result){
                if(result.code==0){
                    $('#ad_edit_modal').modal('hide');
                }else{
                    alert('修改失败');
                }
            });
            return false;    
        }

        $scope.remove = function(ad){
            if(confirm('你确认删除吗？')){
                AdService.remove(ad).success(function(result){
                    if(result.code==0){
                        self.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                    }
                });
            }
        }
    });